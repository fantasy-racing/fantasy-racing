import { Button, Grid } from "@mui/material";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { listChampionships } from "./championshipSlice";
import ChampionshipPaper from "./components/ChampionshipPaper";

const ChampionshipList = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const championships = useSelector(
    (state) => state.championship.championships
  );

  useEffect(() => {
    dispatch(listChampionships());
  }, [dispatch]);

  const handleOnClickChampionship = (id) => {
    navigate(`${id}`);
  };

  const handleOnClickCreateChampionship = () => {
    navigate("create");
  };

  return (
    <>
      <Grid container>
        {championships &&
          championships.map((ch) => (
            <Grid item xs={4}>
              <ChampionshipPaper
                name={ch.name}
                handleOnClickChampionship={handleOnClickChampionship}
                id={ch.id}
                key={ch.id}
              />
            </Grid>
          ))}
        <Grid item>
          <Button onClick={handleOnClickCreateChampionship}>
            Create new Championship
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

ChampionshipList.propTypes = {};

export default ChampionshipList;
