import { Paper, Typography } from "@mui/material";
import React from "react";

const ChampionshipPaper = ({ name, handleOnClickChampionship, id }) => {
  return (
    <>
      <Paper
        onClick={() => handleOnClickChampionship(id)}
        sx={{ width: 150, height: 150 }}
      >
        <Typography>{name}</Typography>
      </Paper>
    </>
  );
};

ChampionshipPaper.propTypes = {};

export default ChampionshipPaper;
