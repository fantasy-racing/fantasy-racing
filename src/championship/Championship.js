import { Button, Grid } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import Calendar from "./calendar/Calendar";
import { getChampionship } from "./championshipSlice";
import Drivers from "./drivers/Drivers";
import Results from "./results/Results";
import Teams from "./teams/Teams";
import FantasyTeam from "./fantasyteam/FantasyTeam";

const Championship = (props) => {
  const params = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const championship = useSelector(
    (state) => state.championship.currentChampionship
  );
  const [editCalendarModalOpen, setEditCalendarModalOpen] = useState();

  useEffect(() => {
    dispatch(getChampionship({ id: params.championshipId }));
  }, [dispatch, params.championshipId]);

  return (
    <>
      <div>{JSON.stringify(championship)}</div>
      <Button onClick={() => setEditCalendarModalOpen(true)}>
        Edit Calendar
      </Button>
      <Button onClick={() => navigate("/app/team/create")}>Create Team</Button>
      <Grid container>
        <Grid item xs={6}>
          <Drivers dispatch={dispatch} />
        </Grid>
        <Grid item xs={6}>
          <Teams dispatch={dispatch} />
        </Grid>
        <Grid item xs={12}>
          {championship?.calendar && (
            <Results dispatch={dispatch} calendar={championship?.calendar} />
          )}
        </Grid>
        <Grid item xs={12}>
          <FantasyTeam dispatch={dispatch} />
        </Grid>
      </Grid>
      {championship?.calendar && editCalendarModalOpen && (
        <Calendar
          editCalendarModalOpen={editCalendarModalOpen}
          setEditCalendarModalOpen={setEditCalendarModalOpen}
          calendar={championship?.calendar}
          dispatch={dispatch}
        />
      )}
    </>
  );
};

Championship.propTypes = {};

export default Championship;
