import { Grid, Paper, Button, Typography } from "@mui/material";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import AddDriverModal from "./AddDriverModal";
import DriverRow from "./DriverRow";
import { createDriver, deleteDriver, editDriver } from "./driversSlice";

const Drivers = ({ dispatch }) => {
  const drivers = useSelector((state) => state.drivers.drivers);
  const [addDriverModalOpen, setAddDriverModalOpen] = useState(false);

  const handleOnClickConfirmCreateDriver = (driver) => {
    dispatch(createDriver(driver));
    setAddDriverModalOpen(false);
  };

  const handleOnClickConfirmEditDriver = (driver) => {
    dispatch(editDriver(driver));
  };

  const handleOnClickConfirmDeleteDriver = (driverId) => {
    dispatch(deleteDriver({ driverId }));
  };
  return (
    <>
      <Paper>
        <Grid container>
          <Grid item xs={12}>
            <Typography>Drivers</Typography>
          </Grid>
          {drivers &&
            drivers.map((driver) => (
              <DriverRow
                key={driver.id}
                driver={driver}
                handleClickConfirmEditDriver={handleOnClickConfirmEditDriver}
                handleClickConfirmDeleteDriver={
                  handleOnClickConfirmDeleteDriver
                }
              />
            ))}
        </Grid>
        <Grid item sx={{ display: "flex", justifyContent: "center" }}>
          <Button
            variant="contained"
            onClick={() => setAddDriverModalOpen(true)}
          >
            Add Driver
          </Button>
        </Grid>
      </Paper>

      <AddDriverModal
        addDriverModalOpen={addDriverModalOpen}
        setAddDriverModalOpen={setAddDriverModalOpen}
        handleClickConfirmAddNewDriver={handleOnClickConfirmCreateDriver}
      />
    </>
  );
};

Drivers.propTypes = {};

export default Drivers;
