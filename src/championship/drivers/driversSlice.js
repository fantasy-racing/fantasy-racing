import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  collection,
  addDoc,
  getDocs,
  doc,
  getDoc,
  updateDoc,
  deleteDoc,
} from "firebase/firestore";
import { getChampionship } from "../championshipSlice";
import { db } from "../../index";

export const createDriver = createAsyncThunk(
  "drivers/createDriver",
  async ({ name, teamName, cost }, thunkAPI) => {
    const {
      drivers: { championshipId },
    } = thunkAPI.getState();
    const driversCollectionRef = collection(
      db,
      "championships",
      championshipId,
      "drivers"
    );
    const driverRef = await addDoc(driversCollectionRef, {
      name,
      cost,
      teamName,
    }).catch((e) => {
      thunkAPI.rejectWithValue(e);
    });
    return { id: driverRef.id, name, cost };
  }
);

export const editDriver = createAsyncThunk(
  "drivers/editDriver",
  async ({ name, cost, teamName, driverId }, thunkAPI) => {
    const {
      drivers: { championshipId },
    } = thunkAPI.getState();
    const driversCollectionRef = doc(
      db,
      "championships",
      championshipId,
      "drivers",
      driverId
    );
    await updateDoc(driversCollectionRef, {
      name,
      cost,
      teamName,
    }).catch((e) => {
      thunkAPI.rejectWithValue(e);
    });
    return { driverId, name, cost };
  }
);

export const deleteDriver = createAsyncThunk(
  "drivers/deleteDriver",
  async ({ driverId }, thunkAPI) => {
    const {
      drivers: { championshipId },
    } = thunkAPI.getState();
    const driversCollectionRef = doc(
      db,
      "championships",
      championshipId,
      "drivers",
      driverId
    );
    await deleteDoc(driversCollectionRef).catch((e) => {
      thunkAPI.rejectWithValue(e);
    });
    return driverId;
  }
);

const initialState = {
  championshipId: null,
  drivers: [],
};

export const driversSlice = createSlice({
  name: "drivers",
  initialState,
  reducers: {
    signIn: (state, action) => {
      state.user = action.payload.user;
      state.token = action.payload.token;
    },
    logout: (state) => {
      state.user = null;
      state.token = null;
    },
  },
  extraReducers: {
    [getChampionship.fulfilled]: (state, action) => {
      state.championshipId = action.payload.id;
      state.drivers = action.payload.drivers;
    },
    [createDriver.fulfilled]: (state, action) => {
      state.drivers.push(action.payload);
    },
    [editDriver.fulfilled]: (state, action) => {
      state.drivers.forEach((driver) => {
        if (driver.id === action.payload.driverId) {
          driver.cost = action.payload.cost;
          driver.teamName = action.payload.teamName;
          driver.name = action.payload.name;
        }
      });
    },
    [deleteDriver.fulfilled]: (state, action) => {
      state.drivers = state.drivers.filter(
        (driver) => driver.id !== action.payload
      );
    },
  },
});

// Action creators are generated for each case reducer function
export const { signIn, logout } = driversSlice.actions;

export default driversSlice.reducer;
