import React, { useState } from "react";
import { Button, Grid, TextField, Typography } from "@mui/material";

const DriverRow = ({
  driver,
  handleClickConfirmEditDriver,
  handleClickConfirmDeleteDriver,
}) => {
  const [isEditing, setIsEditing] = useState(false);
  const [isDeleting, setIsDeleting] = useState(false);
  const [name, setName] = useState(driver.name);
  const [teamName, setTeamName] = useState(driver.teamName);
  const [cost, setCost] = useState(driver.cost);

  const onClickConfirmEditDriver = () => {
    handleClickConfirmEditDriver({ name, teamName, cost, driverId: driver.id });
    setIsEditing(false);
  };

  return (
    <>
      <Grid container item xs={12} alignItems="center">
        {isEditing ? (
          <>
            <Grid item xs={3}>
              <TextField
                name="name"
                label="Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
                fullWidth
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="teamName"
                label="Team Name"
                fullWidth
                value={teamName}
                onChange={(e) => setTeamName(e.target.value)}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="cost"
                label="Cost"
                fullWidth
                value={cost}
                onChange={(e) => setCost(e.target.value)}
              />
            </Grid>
            <Grid item xs={1}>
              <Button onClick={onClickConfirmEditDriver}>Confirm</Button>
            </Grid>
            <Grid item xs={1}>
              <Button onClick={() => setIsEditing(false)}>Cancel</Button>
            </Grid>
          </>
        ) : (
          <>
            <Grid item xs={3}>
              <Typography>{driver.name}</Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography>{driver.teamName}</Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography>{driver.cost}</Typography>
            </Grid>
            {isDeleting ? (
              <>
                <Grid item xs={1}>
                  <Button
                    onClick={() => handleClickConfirmDeleteDriver(driver.id)}
                  >
                    Confirm
                  </Button>
                </Grid>
                <Grid item xs={1}>
                  <Button onClick={() => setIsDeleting(false)}>Cancel</Button>
                </Grid>
              </>
            ) : (
              <>
                <Grid item xs={1}>
                  <Button onClick={() => setIsEditing(true)}>
                    Edit Driver
                  </Button>
                </Grid>
                <Grid item xs={1}>
                  <Button onClick={() => setIsDeleting(true)}>
                    Remove Driver
                  </Button>
                </Grid>
              </>
            )}
          </>
        )}
      </Grid>
    </>
  );
};

DriverRow.propTypes = {};

export default DriverRow;
