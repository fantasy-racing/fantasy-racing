import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
} from "@mui/material";

const AddDriverModal = ({
  addDriverModalOpen,
  setAddDriverModalOpen,
  handleClickConfirmAddNewDriver,
}) => {
  const [name, setName] = useState("");
  const [teamName, setTeamName] = useState("");
  const [cost, setCost] = useState("");

  return (
    <Dialog
      open={addDriverModalOpen}
      onClose={() => setAddDriverModalOpen(false)}
    >
      <DialogTitle>Add new Driver</DialogTitle>
      <DialogContent>
        <Grid container spacing={1} sx={{ mt: 1 }}>
          <Grid item xs={12}>
            <TextField
              name="name"
              label="Name"
              value={name}
              onChange={(e) => setName(e.target.value)}
              fullWidth
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="teamName"
              label="Team Name"
              fullWidth
              value={teamName}
              onChange={(e) => setTeamName(e.target.value)}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="cost"
              label="Cost"
              fullWidth
              value={cost}
              onChange={(e) => setCost(e.target.value)}
            />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button
          variant="contained"
          onClick={() =>
            handleClickConfirmAddNewDriver({ name, teamName, cost })
          }
        >
          Confirm
        </Button>
        <Button onClick={() => setAddDriverModalOpen(false)}>Cancel</Button>
      </DialogActions>
    </Dialog>
  );
};

AddDriverModal.propTypes = {};

export default AddDriverModal;
