import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  collection,
  addDoc,
  getDocs,
  doc,
  getDoc,
  updateDoc,
  deleteDoc,
} from "firebase/firestore";
import { getChampionship } from "../championshipSlice";
import { db } from "../../index";

export const createTeam = createAsyncThunk(
  "team/createTeam",
  async ({ name, cost }, thunkAPI) => {
    const {
      teams: { championshipId },
    } = thunkAPI.getState();
    const teamsCollectionRef = collection(
      db,
      "championships",
      championshipId,
      "teams"
    );
    const teamRef = await addDoc(teamsCollectionRef, {
      name,
      cost,
      drivers: [],
    }).catch((e) => {
      thunkAPI.rejectWithValue(e);
    });
    return { id: teamRef.id, name, cost };
  }
);

export const editTeam = createAsyncThunk(
  "team/editTeam",
  async ({ name, cost, teamId }, thunkAPI) => {
    const {
      teams: { championshipId },
    } = thunkAPI.getState();
    const teamsCollectionRef = doc(
      db,
      "championships",
      championshipId,
      "teams",
      teamId
    );
    await updateDoc(teamsCollectionRef, {
      name,
      cost,
    }).catch((e) => {
      thunkAPI.rejectWithValue(e);
    });
    return { teamId, name, cost };
  }
);

export const deleteTeam = createAsyncThunk(
  "team/deleteTeam",
  async ({ teamId }, thunkAPI) => {
    const {
      teams: { championshipId },
    } = thunkAPI.getState();
    const teamsCollectionRef = doc(
      db,
      "championships",
      championshipId,
      "teams",
      teamId
    );
    await deleteDoc(teamsCollectionRef).catch((e) => {
      thunkAPI.rejectWithValue(e);
    });
    return teamId;
  }
);

const initialState = {
  championshipId: null,
  teams: [],
};

export const teamsSlice = createSlice({
  name: "teams",
  initialState,
  reducers: {
    signIn: (state, action) => {
      state.user = action.payload.user;
      state.token = action.payload.token;
    },
    logout: (state) => {
      state.user = null;
      state.token = null;
    },
  },
  extraReducers: {
    [getChampionship.fulfilled]: (state, action) => {
      state.championshipId = action.payload.id;
      state.teams = action.payload.teams;
    },
    [createTeam.fulfilled]: (state, action) => {
      state.teams.push(action.payload);
    },
    [editTeam.fulfilled]: (state, action) => {
      state.teams.forEach((team) => {
        if (team.id === action.payload.teamId) {
          team.cost = action.payload.cost;
          team.team = action.payload.team;
        }
      });
    },
    [deleteTeam.fulfilled]: (state, action) => {
      state.teams = state.teams.filter((team) => team.id !== action.payload);
    },
  },
});

// Action creators are generated for each case reducer function
export const { signIn, logout } = teamsSlice.actions;

export default teamsSlice.reducer;
