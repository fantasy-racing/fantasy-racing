import React, { useState } from "react";
import PropTypes from "prop-types";
import { Grid, Paper, Button, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import TeamRow from "./TeamRow";
import AddTeamModal from "./AddTeamModal";
import { createTeam, deleteTeam, editTeam } from "./teamsSlice";

const Teams = ({ dispatch }) => {
  const teams = useSelector((state) => state.teams.teams);
  const [addTeamModalOpen, setAddTeamModalOpen] = useState(false);

  const handleOnClickConfirmCreateTeam = (team) => {
    dispatch(createTeam(team));
    setAddTeamModalOpen(false);
  };

  const handleOnClickConfirmEditTeam = (team) => {
    dispatch(editTeam(team));
  };

  const handleOnClickConfirmDeleteTeam = (teamId) => {
    dispatch(deleteTeam({ teamId }));
  };

  return (
    <>
      <Paper>
        <Grid container>
          <Grid item xs={12}>
            <Typography>Teams</Typography>
          </Grid>
          {teams &&
            teams.map((team) => (
              <TeamRow
                key={team.id}
                team={team}
                handleOnClickConfirmEditTeam={handleOnClickConfirmEditTeam}
                handleOnClickConfirmDeleteTeam={handleOnClickConfirmDeleteTeam}
              />
            ))}
        </Grid>
        <Grid item sx={{ display: "flex", justifyContent: "center" }}>
          <Button variant="contained" onClick={() => setAddTeamModalOpen(true)}>
            Add Team
          </Button>
        </Grid>
      </Paper>
      <AddTeamModal
        addTeamModalOpen={addTeamModalOpen}
        setAddTeamModalOpen={setAddTeamModalOpen}
        handleOnClickConfirmCreateTeam={handleOnClickConfirmCreateTeam}
      />
    </>
  );
};

Teams.propTypes = {};

export default Teams;
