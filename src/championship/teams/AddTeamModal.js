import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
} from "@mui/material";

const AddTeamModal = ({
  addTeamModalOpen,
  setAddTeamModalOpen,
  handleOnClickConfirmCreateTeam,
}) => {
  const [name, setName] = useState("");
  const [cost, setCost] = useState("");

  return (
    <Dialog open={addTeamModalOpen} onClose={() => setAddTeamModalOpen(false)}>
      <DialogTitle>Add new Team</DialogTitle>
      <DialogContent>
        <Grid container spacing={1} sx={{ mt: 1 }}>
          <Grid item xs={12}>
            <TextField
              name="name"
              label="Name"
              value={name}
              onChange={(e) => setName(e.target.value)}
              fullWidth
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="cost"
              label="Cost"
              fullWidth
              value={cost}
              onChange={(e) => setCost(e.target.value)}
            />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button
          variant="contained"
          onClick={() => handleOnClickConfirmCreateTeam({ name, cost })}
        >
          Confirm
        </Button>
        <Button onClick={() => setAddTeamModalOpen(false)}>Cancel</Button>
      </DialogActions>
    </Dialog>
  );
};

AddTeamModal.propTypes = {};

export default AddTeamModal;
