import React, { useState } from "react";
import { Button, Grid, TextField, Typography } from "@mui/material";

const TeamRow = ({
  team,
  handleOnClickConfirmEditTeam,
  handleOnClickConfirmDeleteTeam,
}) => {
  const [isEditing, setIsEditing] = useState(false);
  const [isDeleting, setIsDeleting] = useState(false);
  const [name, setName] = useState(team.name);
  const [cost, setCost] = useState(team.cost);

  const onClickConfirmEditTeam = () => {
    handleOnClickConfirmEditTeam({ name, cost, teamId: team.id });
    setIsEditing(false);
  };

  const onClickConfirmDeleteTeam = () => {
    handleOnClickConfirmDeleteTeam(team.id);
  };

  return (
    <>
      <Grid container item xs={12} alignItems="center">
        {isEditing ? (
          <>
            <Grid item xs={4}>
              <TextField
                name="name"
                label="Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
                fullWidth
              />
            </Grid>
            <Grid item xs={2}>
              <TextField
                name="cost"
                label="Cost"
                fullWidth
                value={cost}
                onChange={(e) => setCost(e.target.value)}
              />
            </Grid>
            <Grid item xs={3}>
              <Button onClick={onClickConfirmEditTeam}>Confirm</Button>
            </Grid>
            <Grid item xs={3}>
              <Button onClick={() => setIsEditing(false)}>Cancel</Button>
            </Grid>
          </>
        ) : (
          <>
            <Grid item xs={4}>
              <Typography>{team.name}</Typography>
            </Grid>

            <Grid item xs={2}>
              <Typography>{team.cost}</Typography>
            </Grid>
            {isDeleting ? (
              <>
                <Grid item xs={3}>
                  <Button onClick={onClickConfirmDeleteTeam}>Confirm</Button>
                </Grid>
                <Grid item xs={3}>
                  <Button onClick={() => setIsDeleting(false)}>Cancel</Button>
                </Grid>
              </>
            ) : (
              <>
                <Grid item xs={3}>
                  <Button onClick={() => setIsEditing(true)}>Edit Team</Button>
                </Grid>
                <Grid item xs={3}>
                  <Button onClick={() => setIsDeleting(true)}>
                    Remove Team
                  </Button>
                </Grid>
              </>
            )}
          </>
        )}
      </Grid>
    </>
  );
};

TeamRow.propTypes = {};

export default TeamRow;
