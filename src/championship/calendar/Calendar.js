import React, { useState } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
} from "@mui/material";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import { editChampionshipCalendar } from "../championshipSlice";

const Calendar = ({
  editCalendarModalOpen = false,
  setEditCalendarModalOpen,
  calendar = [],
  dispatch,
}) => {
  const [cal, setCal] = useState(calendar);

  const onClickConfirm = () => {
    dispatch(editChampionshipCalendar({ calendar: cal }));
    setEditCalendarModalOpen(false);
  };

  const handleEditRaceName = (e, i) => {
    const newCal = structuredClone(cal);
    newCal[i] = { ...newCal[i], [e.target.name]: e.target.value };
    setCal(newCal);
  };

  const handleEditCalendar = (date, i) => {
    const newCal = structuredClone(cal);
    newCal[i] = { ...newCal[i], date: date.toString() };
    setCal(newCal);
  };

  const handleClickAddRace = () => {
    setCal([...cal, { name: "", date: "" }]);
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <Dialog
        open={editCalendarModalOpen}
        onClose={() => setEditCalendarModalOpen(false)}
      >
        <DialogTitle>Edit Calendar</DialogTitle>
        <DialogContent>
          <Grid container spacing={1} sx={{ mt: 1 }}>
            {cal.map((race, i) => (
              <Grid container item xs={12} key={i}>
                <Grid item xs={6}>
                  <TextField
                    name="name"
                    label="Name"
                    value={race.name}
                    onChange={(e) => handleEditRaceName(e, i)}
                    fullWidth
                  />
                </Grid>
                <Grid item xs={6}>
                  <DesktopDatePicker
                    type="date"
                    name="date"
                    label="Date"
                    value={race.date}
                    onChange={(e) => handleEditCalendar(e, i)}
                    renderInput={(params) => <TextField {...params} />}
                  />
                </Grid>
              </Grid>
            ))}
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" onClick={handleClickAddRace}>
            Add Race
          </Button>
          <Button variant="contained" onClick={onClickConfirm}>
            Confirm
          </Button>
          <Button onClick={() => setEditCalendarModalOpen(false)}>
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </LocalizationProvider>
  );
};

Calendar.propTypes = {};

export default Calendar;
