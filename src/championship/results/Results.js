import { Grid, Paper, Button, Typography } from "@mui/material";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { createResult } from "../championshipSlice";
import AddResultModal from "./AddResultModal";
import ResultRow from "./ResultRow";
import { deleteResult, editResult } from "./resultsSlice";

const Results = ({ dispatch, calendar }) => {
  const results = useSelector((state) => state.results.results);
  const drivers = useSelector((state) => state.drivers.drivers);
  const [addResultModalOpen, setAddResultModalOpen] = useState(false);

  const rows = calendar.map((race) => ({
    ...race,
    result: results.find((res) => res.id === race.resultId),
  }));

  const handleOnClickConfirmCreateResult = (result) => {
    dispatch(createResult(result));
    setAddResultModalOpen(false);
  };

  const handleOnClickConfirmEditResult = (result) => {
    dispatch(editResult(result));
  };

  const handleOnClickConfirmDeleteResult = (resultId) => {
    dispatch(deleteResult({ resultId }));
  };

  return (
    <>
      <Paper>
        <Grid container>
          <Grid item xs={12}>
            <Typography>Results</Typography>
          </Grid>
          {rows &&
            rows.map((row) => (
              <ResultRow
                key={row.id}
                result={row.result ? row.result : {}}
                raceName={row.name}
                handleClickConfirmEditResult={handleOnClickConfirmEditResult}
                handleClickConfirmDeleteResult={
                  handleOnClickConfirmDeleteResult
                }
              />
            ))}
        </Grid>
        <Grid item sx={{ display: "flex", justifyContent: "center" }}>
          <Button
            variant="contained"
            onClick={() => setAddResultModalOpen(true)}
          >
            Add Result
          </Button>
        </Grid>
      </Paper>

      {addResultModalOpen && calendar && (
        <AddResultModal
          addResultModalOpen={addResultModalOpen}
          setAddResultModalOpen={setAddResultModalOpen}
          handleClickConfirmAddNewResult={handleOnClickConfirmCreateResult}
          drivers={drivers}
          calendar={calendar}
        />
      )}
    </>
  );
};

Results.propTypes = {};

export default Results;
