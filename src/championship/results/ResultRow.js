import React, { useState } from "react";
import { Button, Grid, TextField, Typography } from "@mui/material";

const ResultRow = ({
  result: { qualifyingOrder = [], raceOrder = [] },
  raceName,
  handleClickConfirmEditResult,
  handleClickConfirmDeleteResult,
}) => {
  return (
    <>
      <Grid container item xs={12} alignItems="center">
        <Grid item xs={3}>
          <Typography>{qualifyingOrder[0]?.name}</Typography>
        </Grid>
        <Grid item xs={3}>
          <Typography>{raceOrder[0]?.name}</Typography>
        </Grid>
        <Grid item xs={3}>
          <Typography>{raceName}</Typography>
        </Grid>
      </Grid>
    </>
  );
};

ResultRow.propTypes = {};

export default ResultRow;
