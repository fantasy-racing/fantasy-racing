import React, { Fragment, useEffect, useState } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  Typography,
} from "@mui/material";

const AddResultModal = ({
  addResultModalOpen,
  setAddResultModalOpen,
  handleClickConfirmAddNewResult,
  drivers,
  calendar = [],
}) => {
  const [qualifyingPool, setQualifyingPool] = useState([]);
  const [qualifyingOrder, setQualifyingOrder] = useState([]);

  const [racePool, setRacePool] = useState([]);
  const [raceOrder, setRaceOrder] = useState([]);

  const [selectedRace, setSelectedRace] = useState("");

  useEffect(() => {
    if (drivers) {
      setQualifyingPool([...drivers, { name: "" }]);
      setRacePool([...drivers, { name: "" }]);
      setQualifyingOrder(Array(drivers.length).fill({ name: "n/a" }));
      setRaceOrder(Array(drivers.length).fill({ name: "n/a" }));
    }
  }, [drivers]);

  const handleOnChangeQualifyingOrder = (e, i) => {
    const filteredPool = qualifyingPool.filter(
      (q) => e.target.value.name !== q.name
    );
    if (qualifyingOrder[i].name !== "n/a")
      filteredPool.push(qualifyingOrder[i]);
    setQualifyingPool(filteredPool);

    qualifyingOrder[i] = e.target.value;
    setQualifyingOrder(qualifyingOrder);
  };

  const handleOnChangeRaceOrder = (e, i) => {
    const filteredPool = racePool.filter((r) => e.target.value.name !== r.name);
    if (raceOrder[i].name !== "n/a") filteredPool.push(raceOrder[i]);
    setRacePool(filteredPool);

    raceOrder[i] = e.target.value;
    setRaceOrder(raceOrder);
  };
  return (
    <Dialog
      keepMounted={false}
      open={addResultModalOpen}
      onClose={() => setAddResultModalOpen(false)}
    >
      <DialogTitle>Add new Result</DialogTitle>
      <DialogContent>
        <Grid container>
          <Grid item xs={3} />

          <Grid item xs={6} sx={{ display: "flex", alignItems: "center" }}>
            <FormControl fullWidth>
              <InputLabel>Race</InputLabel>
              <Select
                name={`Race`}
                label={`Race`}
                value={selectedRace}
                onChange={(e) => setSelectedRace(e.target.value)}
                fullWidth
              >
                {calendar.map((race) => {
                  if (!race.resultId) {
                    return (
                      <MenuItem value={race} key={race.name}>
                        {race.name}
                      </MenuItem>
                    );
                  }
                  return null;
                })}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={3} />

          <Grid container item xs={6} spacing={1} sx={{ mt: 1 }}>
            <Grid item xs={12}>
              <Typography>Qualifying Results</Typography>
            </Grid>
            {qualifyingOrder.map((driver, i) => (
              <Fragment key={`qualy${i}`}>
                <Grid item xs={6}>
                  <FormControl fullWidth>
                    <InputLabel>{i + 1}</InputLabel>
                    <Select
                      name={`Qualifying Position ${i}`}
                      label={`${i + 1}`}
                      value={qualifyingOrder[i].name}
                      renderValue={() => qualifyingOrder[i].name}
                      onChange={(e) => handleOnChangeQualifyingOrder(e, i)}
                      fullWidth
                    >
                      {qualifyingPool.map((driver) => (
                        <MenuItem value={driver} key={driver.name}>
                          {driver.name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Grid>
              </Fragment>
            ))}
          </Grid>
          <Grid container item xs={6} spacing={1} sx={{ mt: 1 }}>
            <Grid item xs={12}>
              <Typography>Race Results</Typography>
            </Grid>
            {raceOrder.map((driver, i) => (
              <Grid item xs={6} key={`race${i}`}>
                <FormControl fullWidth>
                  <InputLabel>{i + 1}</InputLabel>
                  <Select
                    name={`Race Position ${i}`}
                    label={`${i + 1}`}
                    value={raceOrder[i].name}
                    renderValue={() => raceOrder[i].name}
                    onChange={(e) => handleOnChangeRaceOrder(e, i)}
                    fullWidth
                  >
                    {racePool.map((driver) => (
                      <MenuItem value={driver}>{driver.name}</MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button
          variant="contained"
          onClick={() =>
            handleClickConfirmAddNewResult({
              qualifyingOrder,
              raceOrder,
              selectedRace,
            })
          }
        >
          Confirm
        </Button>
        <Button onClick={() => setAddResultModalOpen(false)}>Cancel</Button>
      </DialogActions>
    </Dialog>
  );
};

AddResultModal.propTypes = {};

export default AddResultModal;
