import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  collection,
  addDoc,
  getDocs,
  doc,
  getDoc,
  updateDoc,
  deleteDoc,
} from "firebase/firestore";
import { createResult, getChampionship } from "../championshipSlice";
import { db } from "../../index";

export const editResult = createAsyncThunk(
  "results/editResult",
  async ({ name, cost, teamName, resultId }, thunkAPI) => {
    const {
      results: { championshipId },
    } = thunkAPI.getState();
    const resultsCollectionRef = doc(
      db,
      "championships",
      championshipId,
      "results",
      resultId
    );
    await updateDoc(resultsCollectionRef, {
      name,
      cost,
      teamName,
    }).catch((e) => {
      thunkAPI.rejectWithValue(e);
    });
    return { resultId, name, cost };
  }
);

export const deleteResult = createAsyncThunk(
  "results/deleteResult",
  async ({ resultId }, thunkAPI) => {
    const {
      results: { championshipId },
    } = thunkAPI.getState();
    const resultsCollectionRef = doc(
      db,
      "championships",
      championshipId,
      "results",
      resultId
    );
    await deleteDoc(resultsCollectionRef).catch((e) => {
      thunkAPI.rejectWithValue(e);
    });
    return resultId;
  }
);

const initialState = {
  championshipId: null,
  results: [],
};

export const resultsSlice = createSlice({
  name: "results",
  initialState,
  reducers: {
    signIn: (state, action) => {
      state.user = action.payload.user;
      state.token = action.payload.token;
    },
    logout: (state) => {
      state.user = null;
      state.token = null;
    },
  },
  extraReducers: {
    [getChampionship.fulfilled]: (state, action) => {
      state.championshipId = action.payload.id;
      state.results = action.payload.results;
    },
    [createResult.fulfilled]: (state, action) => {
      state.results.push({
        id: action.payload.id,
        qualifyingOrder: action.payload.qualifyingOrder,
        raceOrder: action.payload.raceOrder,
      });
    },
    [editResult.fulfilled]: (state, action) => {
      state.results.forEach((result) => {
        if (result.id === action.payload.resultId) {
          result.cost = action.payload.cost;
          result.teamName = action.payload.teamName;
          result.name = action.payload.name;
        }
      });
    },
    [deleteResult.fulfilled]: (state, action) => {
      state.results = state.results.filter(
        (result) => result.id !== action.payload
      );
    },
  },
});

// Action creators are generated for each case reducer function
export const { signIn, logout } = resultsSlice.actions;

export default resultsSlice.reducer;
