import React from "react";
import PropTypes from "prop-types";
import { Paper, Typography } from "@mui/material";
import styled from "@emotion/styled";

const DriverCardPaper = styled(Paper)(({ theme, driver }) => ({
  width: 125,
  height: 150,
  background: driver ? "white" : "grey",
}));

const DriverCard = ({ driver }) => {
  return (
    <>
      {driver ? (
        <>
          <DriverCardPaper driver={driver}>
            <Typography>{driver.name}</Typography>
          </DriverCardPaper>
        </>
      ) : (
        <>
          <DriverCardPaper driver={driver}>
            <Typography>default</Typography>
          </DriverCardPaper>
        </>
      )}
    </>
  );
};

DriverCard.propTypes = {};

export default DriverCard;
