import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { Grid } from "@mui/material";
import DriverCard from "./components/DriverCard";
import { useSelector } from "react-redux";
import { setFantasyTeam } from "./fantasyTeamSlice";

const Team = ({ dispatch }) => {
  const user = useSelector((state) => state.auth.user);
  const fantasyTeam = useSelector(
    (state) => state.fantasyTeam.currentFantasyTeam
  );
  const fantasyTeamList = useSelector(
    (state) => state.fantasyTeam.fantasyTeams
  );

  useEffect(() => {
    if (user && !fantasyTeam && fantasyTeamList) {
      dispatch(setFantasyTeam(user));
    }
  }, [fantasyTeam, user, dispatch, fantasyTeamList]);
  return (
    <>
      <Grid container>
        {fantasyTeam?.drivers?.map((driver) => (
          <DriverCard driver={driver} />
        ))}
      </Grid>
    </>
  );
};

Team.propTypes = {};

export default Team;
