import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  collection,
  addDoc,
  getDocs,
  doc,
  getDoc,
  updateDoc,
  deleteDoc,
} from "firebase/firestore";
import { getChampionship } from "../championshipSlice";
import { db } from "../../index";

export const createDriver = createAsyncThunk(
  "fantasyTeam/createDriver",
  async ({ name, teamName, cost }, thunkAPI) => {
    const {
      drivers: { championshipId },
    } = thunkAPI.getState();
    const driversCollectionRef = collection(
      db,
      "championships",
      championshipId,
      "drivers"
    );
    const driverRef = await addDoc(driversCollectionRef, {
      name,
      cost,
      teamName,
    }).catch((e) => {
      thunkAPI.rejectWithValue(e);
    });
    return { id: driverRef.id, name, cost };
  }
);

export const editDriver = createAsyncThunk(
  "fantasyTeam/editDriver",
  async ({ name, cost, teamName, driverId }, thunkAPI) => {
    const {
      drivers: { championshipId },
    } = thunkAPI.getState();
    const driversCollectionRef = doc(
      db,
      "championships",
      championshipId,
      "drivers",
      driverId
    );
    await updateDoc(driversCollectionRef, {
      name,
      cost,
      teamName,
    }).catch((e) => {
      thunkAPI.rejectWithValue(e);
    });
    return { driverId, name, cost };
  }
);

export const deleteDriver = createAsyncThunk(
  "fantasyTeam/deleteDriver",
  async ({ driverId }, thunkAPI) => {
    const {
      drivers: { championshipId },
    } = thunkAPI.getState();
    const driversCollectionRef = doc(
      db,
      "championships",
      championshipId,
      "drivers",
      driverId
    );
    await deleteDoc(driversCollectionRef).catch((e) => {
      thunkAPI.rejectWithValue(e);
    });
    return driverId;
  }
);

const initialState = {
  championshipId: null,
  fantasyTeams: [],
};

export const fantasyTeamSlice = createSlice({
  name: "fantasyTeam",
  initialState,
  reducers: {
    setFantasyTeam: (state, action) => {
      state.currentFantasyTeam = state.fantasyTeams.filter(
        (team) => team.id === action.payload
      )[0];
    },
  },
  extraReducers: {
    [getChampionship.fulfilled]: (state, action) => {
      state.championshipId = action.payload.id;
      state.fantasyTeams = action.payload.fantasyTeams;
      state.currentFantasyTeam = action.payload.currentFantasyTeam;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setFantasyTeam } = fantasyTeamSlice.actions;

export default fantasyTeamSlice.reducer;
