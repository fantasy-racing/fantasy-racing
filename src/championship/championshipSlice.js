import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  collection,
  addDoc,
  getDocs,
  doc,
  getDoc,
  updateDoc,
  deleteDoc,
  Timestamp,
} from "firebase/firestore";
import { db } from "../index";

export const createResult = createAsyncThunk(
  "results/createResult",
  async ({ qualifyingOrder, raceOrder, selectedRace }, thunkAPI) => {
    const {
      results: { championshipId },
      championship: {
        currentChampionship: { calendar },
      },
    } = thunkAPI.getState();
    const resultsCollectionRef = collection(
      db,
      "championships",
      championshipId,
      "results"
    );
    const filteredQualifyingOrder = qualifyingOrder.filter(
      (position) => position.name !== "n/a"
    );

    const filteredRaceOrder = raceOrder.filter(
      (position) => position.name !== "n/a"
    );
    if (
      filteredQualifyingOrder.length === 0 ||
      filteredQualifyingOrder.length === 0
    ) {
      return thunkAPI.rejectWithValue(
        "Must have at least one participant in qualifying and race"
      );
    }

    const resultRef = await addDoc(resultsCollectionRef, {
      qualifyingOrder: filteredQualifyingOrder,
      raceOrder: filteredRaceOrder,
      raceName: selectedRace.name,
    }).catch((e) => {
      return thunkAPI.rejectWithValue(e);
    });

    const championshipRef = doc(db, "championships", championshipId);
    const updatedCalendar = calendar.map((race) =>
      race.name === selectedRace.name
        ? { ...race, resultId: resultRef.id }
        : race
    );

    await updateDoc(championshipRef, { calendar: updatedCalendar }).catch(
      (e) => {
        return thunkAPI.rejectWithValue(e);
      }
    );

    return {
      id: resultRef.id,
      qualifyingOrder: filteredQualifyingOrder,
      raceOrder: filteredRaceOrder,
      calendar: updatedCalendar,
    };
  }
);
export const createChampionship = createAsyncThunk(
  "championship/createChampionship",
  async ({ name }, thunkAPI) => {
    await addDoc(collection(db, "championships"), {
      name,
      teams: [],
      drivers: [],
    }).catch((e) => {
      thunkAPI.rejectWithValue(e);
    });
  }
);

// TODO: trim this down to exclude useless data for the list page
export const listChampionships = createAsyncThunk(
  "championship/listChampionship",
  async (data, thunkAPI) => {
    const querySnapshot = await getDocs(collection(db, "championships")).catch(
      (e) => {
        thunkAPI.rejectWithValue(e);
      }
    );
    const results = querySnapshot.docs.map((doc) => {
      return { ...doc.data(), id: doc.id };
    });
    return results;
  }
);

export const getChampionship = createAsyncThunk(
  "championship/getChampionship",
  async ({ id }, thunkAPI) => {
    const userId = await thunkAPI.getState().auth.user;
    const championshipDocRef = doc(db, "championships", id);
    const driversCollectionRef = collection(db, "championships", id, "drivers");
    const teamsCollectionRef = collection(db, "championships", id, "teams");
    const resultsCollectionRef = collection(db, "championships", id, "results");
    const fantasyTeamsCollectionRef = collection(
      db,
      "championships",
      id,
      "fantasy-teams"
    );

    const championShipDocSnap = await getDoc(championshipDocRef);
    const driversCollectionSnap = await getDocs(driversCollectionRef);
    const teamsCollectionSnap = await getDocs(teamsCollectionRef);
    const resultsCollectionSnap = await getDocs(resultsCollectionRef);
    const fantasyTeamsCollectionSnap = await getDocs(fantasyTeamsCollectionRef);

    const drivers = driversCollectionSnap.docs.map((driver) => ({
      id: driver.id,
      ...driver.data(),
    }));

    const teams = teamsCollectionSnap.docs.map((team) => ({
      id: team.id,
      ...team.data(),
    }));

    const results = resultsCollectionSnap.docs.map((result) => ({
      id: result.id,
      ...result.data(),
    }));

    const fantasyTeams = fantasyTeamsCollectionSnap.docs.map((fantasyTeam) => ({
      id: fantasyTeam.id,
      ...fantasyTeam.data(),
    }));

    let result;
    if (championShipDocSnap.exists()) {
      result = {
        ...championShipDocSnap.data(),
        id: championShipDocSnap.id,
      };
    } else {
      thunkAPI.rejectWithValue("doc doesn't exist");
    }

    return {
      ...result,
      drivers,
      teams,
      results,
      fantasyTeams,
    };
  }
);

export const addDriverToChampionship = createAsyncThunk(
  "championship/addDriverToChampionship",
  async ({ id, name, teamName, cost }, thunkAPI) => {
    const driverCollectionRef = collection(db, "championships", id, "drivers");
    const driverRef = await addDoc(driverCollectionRef, {
      name,
      teamName,
      cost,
    }).catch((e) => {
      thunkAPI.rejectWithValue(e);
    });
    return { id: driverRef.id, name, teamName, cost };
  }
);

export const editDriverInChampionship = createAsyncThunk(
  "championship/editDriverInChampionship",
  async ({ id, name, teamName, cost, driverId }, thunkAPI) => {
    const driverDocRef = doc(db, "championships", id, "drivers", driverId);
    await updateDoc(driverDocRef, {
      name,
      teamName,
      cost,
    }).catch((e) => {
      thunkAPI.rejectWithValue(e);
    });
    return { driverId, name, teamName, cost };
  }
);

export const deleteDriverFromChampionship = createAsyncThunk(
  "championship/deleteDriverFromChampionship",
  async ({ id, driverId }, thunkAPI) => {
    const driverCollectionRef = doc(
      db,
      "championships",
      id,
      "drivers",
      driverId
    );
    await deleteDoc(driverCollectionRef).catch((e) => {
      thunkAPI.rejectWithValue(e);
    });
    return driverId;
  }
);

export const editChampionshipCalendar = createAsyncThunk(
  "championship/editChampionshipCalendar",
  async ({ calendar }, thunkAPI) => {
    const {
      championship: {
        currentChampionship: { id: championshipId },
      },
    } = thunkAPI.getState();

    const championshipRef = doc(db, "championships", championshipId);
    await updateDoc(championshipRef, { calendar }).catch((e) => {
      return thunkAPI.rejectWithValue(e);
    });
    return calendar;
  }
);

const initialState = {
  championships: null,
  currentChampionship: null,
};

export const championshipSlice = createSlice({
  name: "championship",
  initialState,
  reducers: {
    signIn: (state, action) => {
      state.user = action.payload.user;
      state.token = action.payload.token;
    },
    logout: (state) => {
      state.user = null;
      state.token = null;
    },
  },
  extraReducers: {
    [listChampionships.fulfilled]: (state, action) => {
      state.championships = action.payload;
    },
    [getChampionship.fulfilled]: (state, action) => {
      state.currentChampionship = action.payload;
    },
    [addDriverToChampionship.fulfilled]: (state, action) => {
      state.currentChampionship.drivers.push(action.payload);
    },
    [editDriverInChampionship.fulfilled]: (state, action) => {
      state.currentChampionship.drivers.forEach((driver) => {
        if (driver.id === action.payload.driverId) {
          driver.cost = action.payload.cost;
          driver.teamName = action.payload.teamName;
          driver.team = action.payload.team;
        }
      });
    },
    [deleteDriverFromChampionship.fulfilled]: (state, action) => {
      state.currentChampionship.drivers =
        state.currentChampionship.drivers.filter(
          (driver) => driver.id !== action.payload
        );
    },
    [editChampionshipCalendar.fulfilled]: (state, action) => {
      state.currentChampionship.calendar = action.payload;
    },
    [createResult.fulfilled]: (state, action) => {
      state.currentChampionship.calendar = action.payload.calendar;
    },
  },
});

// Action creators are generated for each case reducer function
export const { signIn, logout } = championshipSlice.actions;

export default championshipSlice.reducer;
