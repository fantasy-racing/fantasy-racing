import { configureStore } from "@reduxjs/toolkit";
import authSlice from "./auth/authSlice";
import championshipSlice from "./championship/championshipSlice";
import driversSlice from "./championship/drivers/driversSlice";
import fantasyTeamSlice from "./championship/fantasyteam/fantasyTeamSlice";
import resultsSlice from "./championship/results/resultsSlice";
import teamsSlice from "./championship/teams/teamsSlice";

export const store = configureStore({
  reducer: {
    auth: authSlice,
    championship: championshipSlice,
    teams: teamsSlice,
    drivers: driversSlice,
    results: resultsSlice,
    fantasyTeam: fantasyTeamSlice,
  },
});
