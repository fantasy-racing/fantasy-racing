import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

const initialState = {
  user: null,
  token: null,
};

export const signUp = createAsyncThunk("auth/signup", async () => {});

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    signIn: (state, action) => {
      state.user = action.payload.user;
      state.token = action.payload.token;
    },
    logout: (state) => {
      state.user = null;
      state.token = null;
    },
    setUser: (state, action) => {
      state.user = action.payload.user;
      state.token = action.payload.token;
    },
  },
  extraReducers: {
    [signUp.fulfilled]: (state, action) => {
      state.user = action.user.uid;
      state.token = action.token;
    },
  },
});

// Action creators are generated for each case reducer function
export const { signIn, logout, setUser } = authSlice.actions;

export default authSlice.reducer;
