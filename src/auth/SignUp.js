import React from "react";
import PropTypes from "prop-types";
import { Button, Grid, Paper, TextField } from "@mui/material";
import { getAuth, GoogleAuthProvider, signInWithPopup } from "firebase/auth";
const provider = new GoogleAuthProvider();

const SignUp = (props) => {
  const auth = getAuth();

  const handleGoogleSignUp = () => {
    signInWithPopup(auth, provider)
      .then((result) => {
        // This gives you a Google Access Token. You can use it to access the Google API.
        const credential = GoogleAuthProvider.credentialFromResult(result);
        const token = credential.accessToken;
        // The signed-in user info.
        const user = result.user;
        // ...
        return { token, user };
      })
      .catch((error) => {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        // The email of the user's account used.
        const email = error.email;
        // The AuthCredential type that was used.
        const credential = GoogleAuthProvider.credentialFromError(error);
        // ...
      });
  };

  return (
    <>
      <Paper
        sx={{
          width: "40%",
          height: 300,
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Grid container alignItems="center" justifyContent="center" spacing={1}>
          <Grid item xs={3} />
          <Grid item xs={6}>
            <TextField fullWidth name="Email" label="Email" />
          </Grid>
          <Grid item xs={3} />
          <Grid item xs={3} />

          <Grid item xs={6}>
            <TextField fullWidth name="Password" label="Password" />
          </Grid>
          <Grid item xs={3} />
          <Grid item xs={3} />
          <Grid item xs={6} sx={{ display: "flex", justifyContent: "center" }}>
            <Button>Sign Up</Button>
          </Grid>
          <Grid item xs={3} />
          <Grid item xs={3} />
          <Grid
            item
            xs={6}
            sx={{ display: "flex", justifyContent: "center" }}
            onClick={handleGoogleSignUp}
          >
            <Button>Sign up with Google</Button>
          </Grid>
          <Grid item xs={3} />
        </Grid>
      </Paper>
    </>
  );
};

SignUp.propTypes = {};

export default SignUp;
