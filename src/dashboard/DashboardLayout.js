import React, { useEffect } from "react";
import { Box, Button, Drawer, List, ListItem } from "@mui/material";
import { Link, Outlet, useNavigate } from "react-router-dom";
import { getAuth, signOut } from "firebase/auth";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../auth/authSlice";

const DashboardLayout = (props) => {
  const auth = getAuth();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const user = useSelector((state) => state.auth.user);
  const handleClickSignOut = () => {
    signOut(auth)
      .then(() => {
        dispatch(logout());
      })
      .catch((error) => {
        // An error happened.
      });
  };

  // useEffect(() => {
  //   if (!user) navigate("/login");
  // }, [navigate, user]);

  return (
    <>
      <Box
        display="flex"
        sx={{ width: "100%", height: "100vh", background: "#BFC8DE" }}
      >
        <Drawer
          variant="permanent"
          sx={{
            width: 150,
            "& .MuiDrawer-paper": {
              width: 150,
            },
          }}
        >
          <List>
            <ListItem>
              <Link to="championship">Championship</Link>
            </ListItem>
            <ListItem>
              <Button onClick={handleClickSignOut}>Sign Out</Button>
            </ListItem>
          </List>
        </Drawer>
        <Box>
          <Outlet />
        </Box>
      </Box>
    </>
  );
};

DashboardLayout.propTypes = {};

export default DashboardLayout;
