import { Navigate, useRoutes } from "react-router-dom";
import Login from "./auth/Login";
import SignUp from "./auth/SignUp";
import DashboardLayout from "./dashboard/DashboardLayout";
import { useSelector } from "react-redux";
import ChampionshipList from "./championship/ChampionshipList";
import CreateChampionship from "./championship/CreateChampionship";
import Championship from "./championship/Championship";
import { setUser } from "./auth/authSlice";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { useDispatch } from "react-redux";
import CreateTeam from "./championship/fantasyteam/CreateTeam";
import Team from "./championship/fantasyteam/FantasyTeam";

function App() {
  const user = useSelector((state) => state.auth.user);
  const dispatch = useDispatch();
  const auth = getAuth();
  onAuthStateChanged(auth, (u) => {
    if (u && !user) {
      dispatch(setUser({ user: u.uid, token: "" }));
    }
  });
  let element = useRoutes([
    {
      path: "/",
      element: <Navigate to="/app" replace />,
    },
    {
      path: "app",
      element: <DashboardLayout />,
      children: [
        {
          path: "championship",
          children: [
            {
              index: true,
              element: <ChampionshipList />,
            },
            {
              path: "create",
              element: <CreateChampionship />,
            },
            {
              path: ":championshipId",
              element: <Championship />,
              // children: [
              //   {
              //     path: "team",
              //     children: [
              //       {
              //         index: true,
              //         element: <Team />,
              //       },
              //       {
              //         path: "create",
              //         element: <CreateTeam />,
              //       },
              //     ],
              //   },
              // ],
            },
          ],
        },
      ],
    },
    {
      path: "login",
      element: user ? <Navigate to="/app" replace /> : <Login />,
    },
    {
      path: "signup",
      element: user ? <Navigate to="/app" replace /> : <SignUp />,
    },
  ]);

  return element;

  // return (
  //   <Routes>
  //     <Route path="/" element={<Navigate to="/app" replace />} />
  //     <Route path="signup" element={<SignUp />} />
  //     <Route
  //       path="login"
  //       element={user ? <Navigate to="/app" replace /> : <Login />}
  //     />
  //     <Route path="app" element={<DashboardLayout />}>
  //       <Route path="team" element={<Team />} />
  //       <Route path="championship" element={<ChampionshipList />} />

  //       {/* <Route path="championship">
  //         <Route path="" element={<ChampionshipList />} />
  //         <Route path="create" element={<CreateChampionship />} />
  //       </Route> */}
  //     </Route>
  //     <Route path="*" element={<Navigate to="/app" replace />} />
  //   </Routes>
  // );
}

export default App;
